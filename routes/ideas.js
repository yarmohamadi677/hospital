const contentTypeParser = require("content-type-parser");
const express = require("express");
const { authenticated } = require("../middlewares/auth");
const { ideaCheckValidator } = require("../validators/Idea-Validator");
const Idea = require("../models/idea");
const Field = require("../models/field");
const basicAuth = require("../middlewares/basic-auth");
const nextify = require("../lib/nextify");
const validate = require("../validators");
const appRoot = require("app-root-path");
const shortId = require("shortid");
let formidable = require("formidable");
let fs = require("fs");
const { date } = require("yup");
const { time, timeNew } = require("../utils/timeOut");

const ideasRouter = express.Router();

/*****    /form */
/** GET Method for /form */
ideasRouter.get(
  "/form",
  authenticated,
  nextify(async (req, res) => {
    const Fields = await Field.query();
    if (time < timeNew) {
      return res.redirect("/404");
    }
    res.render("./form.ejs", {
      pageTitle: "فرم ثبت ایده",
      firstName: req.user.firstName,
      lastName: req.user.lastName,
      office: req.user.office,
      userId: req.user.userId,
      title: req.user.title,
      AccessLevel: req.user.AccessLevel,

      Fields,
      Idea,
    });
  })
);
/** POST Method for /form */
ideasRouter.post(
  "/form",
  authenticated,
  validate.body({
    title: "title|optional",
    fieldId: "fieldId|optional",
    keyWords: "keyWords|optional",
    challenge: "challenge|optional",
    description: "description|optional",
    timeAndPlace: "timeAndPlace|optional",
    result: "result|optional",
    recommendation: "recommendation|optional",
    attachFile: "attachFile|optional",
  }),
  nextify(async (req, res) => {
    const attachFile = req.files ? req.files.attachFile : {};
    const fileName = `${shortId.generate()}_${attachFile.name}`;
    const uploadPath = `${appRoot}/public/uploads/attachFiles/${fileName}`;
    req.body = { ...req.body, createdAt: Date(), attachFile: fileName };
    const ideaInfo = req.body;
    const validate = ideaInfo;
    if (validate) {
      if (req.files) {
        await attachFile.mv(uploadPath, function (err) {
          if (err) return res.status(500).send(err);
          return res.status(200);
        });
      }
      await Idea.query().insert(ideaInfo);
      res.redirect("/dashboard");
    } else {
      res.redirect("/form");
    }
  })
);

/** DELETE Method */
ideasRouter.get(
  "/del/:id",
  authenticated,
  nextify(async (req, res) => {
    const id = req.params.id;
    const idea = await Idea.query().findById(id);
    if (time < timeNew) {
      return res.redirect("/404");
    }

    if (idea == null) {
      return res.status(404).json({
        message: "idea Not Found",
      });
    } else {
      await Idea.query().deleteById(id);
      res.redirect("/dashboard");
    }
  })
);

/** ViewDetails Method */
ideasRouter.get(
  "/ViewDetails/:id",
  authenticated,
  nextify(async (req, res) => {
    const id = req.params.id;
    const patch = req.body;
    const Fields = await Field.query();

    const mainQuery = Idea.query()
      .findById(id)
      .select("field.*", "ideas.*")
      .joinRelated("field");

    const idea = await Idea.query().findById(id);
    if (idea == null) {
      return res.status(404).json({
        message: "PATCH Not Found",
      });
    } else {
      res.render("./ViewDetails.ejs", {
        pageTitle: "مشاهده",
        idea,
        mainQuery,
        firstName: req.user.firstName,
        lastName: req.user.lastName,
        office: req.user.office,
        userId: req.user.userId,
        title: req.user.title,
        AccessLevel: req.user.AccessLevel,
        Fields,
      });
    }
  })
);

/** PATCH Method */
ideasRouter.get(
  "/edit/:id",
  authenticated,
  nextify(async (req, res) => {
    const id = req.params.id;
    const patch = req.body;
    const idea = await Idea.query().findById(id);
    const Fields = await Field.query();
    if (time < timeNew) {
      return res.redirect("/404");
    }

    if (idea == null) {
      return res.status(404).json({
        message: "PATCH Not Found",
      });
    } else {
      res.render("./edit.ejs", {
        pageTitle: "ویرایش",
        idea,
        firstName: req.user.firstName,
        lastName: req.user.lastName,
        office: req.user.office,
        title: req.user.title,
        AccessLevel: req.user.AccessLevel,
        Fields,
      });
    }
  })
);

ideasRouter.post(
  "/edit/:id",
  authenticated,
  nextify(async (req, res) => {
    const id = req.params.id;
    const idea = await Idea.query().findById(id);
    const attachFile = req.files ? req.files.attachFile : {};
    const fileName = `${shortId.generate()}_${attachFile.name}`;
    const uploadPath = `${appRoot}/public/uploads/attachFiles/${fileName}`;
    req.body = { ...req.body, createdAt: Date(), attachFile: fileName };
    const ideaInfo = req.body;
    if (attachFile.name) {
      fs.unlink(
        `${appRoot}/public/uploads/attachFiles/${idea.attachFile}`,
        async (err) => {
          if (err) {
            console.log(err);
          } else {
            await attachFile.mv(uploadPath, function (err) {
              if (err) {
                return res.status(500).send(err);
              } else {
                return;
              }
            });
          }
        }
      );
    } else {
      console.log("فایل قبلی بود");
    }
    if (attachFile.name) {
      await Idea.query().findById(id).patch(ideaInfo);
    } else {
      ideaInfo.attachFile = idea.attachFile;
      await Idea.query().findById(id).patch(ideaInfo);
    }

    res.redirect("/dashboard");
  })
);

module.exports = ideasRouter;
