const passport = require("passport");
const { Strategy } = require("passport-local");
const bcrypt = require("bcryptjs");
const User = require("../models/user");
passport.use(
  new Strategy(
    {
      usernameField: "userName",
    },

    async (userName, password, done) => {
      try {
        const user = await User.query().findOne({ userName: userName });
        if (!user) {
          return done(null, false, {
            message: "کاربری با این نام ثبت نشده است",
          });
        }
        const isMatch = await bcrypt.compare(password, user.password);
        if (isMatch) {
          return done(null, user);
        }
        return done(null, false, {
          message: "نام کاربری یا کلمه عبور صحیح نمی باشد.",
        });
      } catch (err) {
        console.log(err);
      }
    }
  )
);

passport.serializeUser(function (user, done) {
  done(null, user.userId);
});

passport.deserializeUser(function (id, done) {
  User.query()
    .findById(id)
    .then(function (user) {
      done(null, user);
    });
});
