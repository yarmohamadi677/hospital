const bcrypt = require("bcryptjs");
const passport = require("passport");
const nextify = require("../lib/nextify");
const { authenticated, adminAuthenticated } = require("../middlewares/auth");
const User = require("../models/User");
const Idea = require("../models/Idea");
const Field = require("../models/Field");

exports.login = nextify(async (req, res) => {
  res.render("login", {
    pageTitle: "ورود به بخش مدیریت",
    path: "/login",
    message: req.flash("success_msg"),
    error: req.flash("error"),
  });
});

exports.handleLogin = async (req, res, next) => {
  passport.authenticate("local", {
    successRedirect: "/dashboard",
    failureRedirect: "/",
    failureFlash: true,
  })(req, res, next);
};

exports.changePass = authenticated,nextify(async (req, res) => {
  res.render("changePass", {
    pageTitle: "تغییر گذر واژه",
    path: "/changePass",
    message: req.flash("success_msg"),
    error: req.flash("error"),
    firstName: req.user.firstName,
    lastName: req.user.lastName,
    userId: req.user.id,
    role: req.user.AccessLevel,
    office: req.user.office,
    userName: req.user.userName,
    AccessLevel: req.user.AccessLevel,
  });
});

exports.handleChangePass =  async (req, res, next) => {
  const username = {
    userId: req.user.userId,
    userName: req.user.userName,
    newpassword: req.body.newpassword,
    confermNewpassword: req.body.confermNewpassword,
    AccessLevel: req.user.AccessLevel,
  };
  if (username.newpassword !== username.confermNewpassword) {
    res.render("changePass", {
      pageTitle: "تغییر گذر واژه",
      error: "گذر واژه جدید و تکرار آن با هم یکسان نیستند",
      message: "",
      path: "/changePass",
      firstName: req.user.firstName,
      lastName: req.user.lastName,
      userId: req.user.id,
      role: req.user.AccessLevel,
      office: req.user.office,
      userName: req.user.userName,
      title: req.user.title,
      AccessLevel: req.user.AccessLevel,
    });
  } else {
    await User.query()
      .findById(username.userId)
      .patch({
        password: bcrypt.hashSync(username.newpassword),
        isActive: true,
      })
      .then((password, err) => {
        if (err) {
          console.log(err);
        }
        req.logout((err) => {
          if (err) {
            return next(err);
          }
          res.render("login", {
            pageTitle: "ورود",
            error: "",
            message:
              "تغییر گذر واژه با موفقیت انجام شد. لطفاً دوباره وارد شوید",
            path: "/changePass",
          });
        });
      });
  }
};
exports.changePassAdmin =  authenticated,nextify(async (req, res) => {
  let id = req.params.id;
  let user = await User.query().findById(id);
  res.render("changePassAdmin", {
    pageTitle: "تغییر گذر واژه",
    path: "/changePassAdmin",
    message: req.flash("success_msg"),
    error: req.flash("error"),
    firstName: req.user.firstName,
    lastName: req.user.lastName,
    userId: req.user.id,
    role: req.user.AccessLevel,
    office: req.user.office,
    userName: req.user.userName,
    title: req.user.title,
    AccessLevel: req.user.AccessLevel,
    user,
  });
});

exports.handleChangePassAdmin = authenticated,nextify(async (req, res) => {
  let newpassword = req.body.newpasswordAdmin;
  let id = req.params.id

  let IDs = req.body.Ids;
  let confermNewpassword = req.body.confermNewpasswordAdmin;
  if (newpassword !== confermNewpassword) {
    let user = await User.query().findById(id);

    res.render("changePassAdmin", {
      pageTitle: "تغییر گذر واژه",
      error: "گذر واژه جدید و تکرار آن با هم یکسان نیستند",
      message: "",
      path: "/changePassAdmin",
      firstName: req.user.firstName,
      lastName: req.user.lastName,
      userId: req.user.id,
      role: req.user.AccessLevel,
      office: req.user.office,
      userName: req.user.userName,
      title: req.user.title,
      AccessLevel: req.body.AccessLevel,
      user,
    });
  } else {
    const Users = await User.query();
    if (!Users) {
      return res.send(err);
    }
    const id = req.body.Ids;
    console.log(id)
    await User.query()
      .findById(id)
      .patch({
        password: bcrypt.hashSync(newpassword),
        isActive: !req.body.isActive,
      })
      .then((password, err) => {
        if (err) {
          console.log(err);
        }
        res.render("dashboardAdmin", {
          pageTitle: "پنل مدیریت",
          error: "",
          message: "تغییر گذر واژه با موفقیت انجام شد. ",
          path: "/dashboardAdmin",
          Users,
          firstName: req.user.firstName,
          lastName: req.user.lastName,
          userId: req.user.id,
          role: req.user.AccessLevel,
          office: req.user.office,
          userName: req.user.userName,
          title: req.user.title,
          AccessLevel: req.user.AccessLevel,
        });
      });
  }
});


exports.logout = nextify(async (req, res, next) => {
  req.logout((err) => {
    if (err) {
      return next(err);
    }
    req.flash("success_msg", "خروج موفقیت آمیز بود");
    res.redirect("/");
  });
});

exports.insertUser = authenticated,nextify(async (req, res, next) => {
  const Fields = await Field.query();
  res.render("insertUser", {
    path: "/insertUser",
    pageTitle: "ساخت کاربر جدید",
    error: "",
    message: "",
    firstName: req.user.firstName,
    lastName: req.user.lastName,
    userId: req.user.id,
    role: req.user.AccessLevel,
    office: req.user.office,
    userName: req.user.userName,
    title: req.user.title,
    AccessLevel: req.user.AccessLevel,
    Fields,
  });
});
