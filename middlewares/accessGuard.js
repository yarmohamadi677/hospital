/** accessGuard Middelware */
const nextify = require("../lib/nextify");
const Idea = require("../models/idea");

const accessGuard = nextify(async (req, res, next) => {
  if (!req.user) {
    return;
  }
  const idea = await Idea.query()
  req.idea = idea;
  return next();
});
module.exports = accessGuard;
