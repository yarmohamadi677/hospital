exports.authenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }

  res.redirect("/404");
};

exports.adminAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    if (req.user) {
      if (req.user.AccessLevel == 'admin') {
        return next();
      } else {
        console.log("user not is admin")
        res.redirect("/404");
      }
    } else {
      console.log("user Is Not Authenticated")
      res.redirect("/404");
    }
  }
  res.redirect("/404");
};
