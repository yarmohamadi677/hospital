const express = require("express");
const fileUpload = require("express-fileupload");
const path = require("path");
const passport = require("passport");
const session = require("express-session");

const flash = require("connect-flash");
const bodyParser = require("body-parser");

const helmet = require("helmet");
const nextify = require("./lib/nextify");
const errorHandler = require("./middlewares/error-handler");
const peopleRouter = require("./routes/people");
const ideasRouter = require("./routes/ideas");
const userController = require("./controllers/userController");
const Idea = require("./models/idea");
const User = require("./models/user");
const Field = require("./models/field");
const { adminAuthenticated, authenticated } = require("./middlewares/auth");
const accessGuard = require("./middlewares/accessGuard");
const { formatDate } = require("./utils/jalali");
const { time, timeNew } = require("./utils/timeOut");
//* Passport Configuration
require("./config/passport");
const app = express();
app.use(bodyParser());
app.use(express.static(path.join(__dirname, "public")));
app.set("view engine", "ejs");
app.use(helmet.hidePoweredBy());

//*fileUpload Middleware */
app.use(fileUpload());

//*session */
app.use(
  session({
    secret: "secret",
    cookie: { maxAge: 3000000 },
    resave: false,
    saveUninitialized: false,
  })
);

app.use(passport.initialize());
app.use(passport.session());

//* Flash
app.use(flash()); //req.flash

const PORT = 3000;

app.use("/people", peopleRouter);
app.use("/", ideasRouter);

/** GET Methods */
app.get("/", (req, res) => {
  if (req.user) {
    res.redirect("/dashboard");
  } else {
    res.render("./login.ejs", {
      pageTitle: "صفحه ورود",
      message: req.flash("success_msg"),
      error: req.flash("error"),
    });
  }
});

/** 404Page */
app.get("/404", (req, res) => {
  res.render("./404.ejs", {
    pageTitle: "404",
  });
});
app.post("/upload", function (req, res) {});

/** dashboardAdmin Page */
app.get(
  "/dashboardAdmin",
  adminAuthenticated,
  authenticated,
  nextify(async (req, res) => {
    let Users = await User.query();
    res.render("./dashboardAdmin.ejs", {
      pageTitle: "پنل مدیریت",
      firstName: req.user.firstName,
      lastName: req.user.lastName,
      userId: req.user.id,
      role: req.user.AccessLevel,
      office: req.user.office,
      userName: req.user.userName,
      title: req.user.title,
      AccessLevel: req.user.AccessLevel,
      Users,
    });
  })
);

/** dashboard Page */
app.get(
  "/dashboard",
  authenticated,
  accessGuard,
  nextify(async (req, res) => {
    let mainQuery = Idea.query()
      .select("field.*", "ideas.*")
      .joinRelated("field");
    let fieldQuery = [];
    /**   Admin Users */
    if (req.user.AccessLevel === "admin") {
      fieldQuery = await mainQuery;
    } else if (req.user.AccessLevel == "fieldAdmin") {
      /**   fieldAdmin Users */
      fieldQuery = await mainQuery.where(
        "ideas.fieldId",
        req.user.fieldAccessId
      );
    } else if (req.user.AccessLevel === "user") {
      /**   User Users */
      fieldQuery = await mainQuery.where("ideas.ownerId", req.user.userId);
    }
    if (req.user.isActive) {
      let edit = null;
      if (time > timeNew) {
        edit = "disabled";
      }

      res.render("./dashboard.ejs", {
        pageTitle: "داشبورد من",
        message: "ورود موفقیت آمیز بود",
        error: "",
        firstName: req.user.firstName,
        lastName: req.user.lastName,
        userId: req.user.id,
        role: req.user.AccessLevel,
        office: req.user.office,
        userName: req.user.userName,
        title: req.user.title,
        AccessLevel: req.user.AccessLevel,
        ideaId: fieldQuery.ideaId,
        formatDate,
        fieldQuery,
        edit,
      });
    } else {
      res.render("./changePass.ejs", {
        pageTitle: "تغییر گذرواژه",
        message: "",
        error: "لطفاً در اولین ورود گذرواژه خود را تغییر دهید",
        firstName: req.user.firstName,
        lastName: req.user.lastName,
        userId: req.user.id,
        role: req.user.AccessLevel,
        office: req.user.office,
        userName: req.user.userName,
        title: req.user.title,
        AccessLevel: req.user.AccessLevel,
        ideaId: fieldQuery.ideaId,
        formatDate,
        fieldQuery,
      });
    }
  })
);

app.get("/login", userController.login);
app.post("/login", userController.handleLogin);
app.get("/logout", authenticated, userController.logout);
app.get("/changePass", authenticated, userController.changePass);
app.post("/changePass", authenticated, userController.handleChangePass);
app.get("/changePassAdmin/:id", authenticated, userController.changePassAdmin);
app.post(
  "/handleChangePassAdmin",
  authenticated,
  userController.handleChangePassAdmin
);
app.get("/insertUser", authenticated, userController.insertUser);
app.use(errorHandler);

app.listen(PORT, () => {
  console.log(`app listening on PORT ${PORT}`);
});
