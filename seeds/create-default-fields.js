/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex("fields").del();
  await knex("fields").insert([
    {
      fieldValue: "ict",
      fieldText: "حوزه فناوری اطلاعات و ارتباطات",
    },
    {
      fieldValue: "treatment",
      fieldText: "حوزه درمان",
    },
    {
      fieldValue: "medicine",
      fieldText: "حوزه غذا و دارو",
    },
    {
      fieldValue: "development",
      fieldText: "حوزه توسعه مدیریت ومنابع",
    },
    {
      fieldValue: "opinionPull",
      fieldText: "حوزه افکارسنجی",
    },
    {
      fieldValue: "health",
      fieldText: "حوزه بهداشت",
    },
  ]);
};
