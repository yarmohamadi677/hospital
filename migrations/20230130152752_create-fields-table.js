
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = (knex) =>
  knex.schema.createTableIfNotExists("fields", (fields) => {
    fields.increments("fieldId").notNullable();
    fields.string("fieldValue").notNullable();
    fields.string("fieldText").notNullable();
  });

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = (knex) => {
  knex.schema.dropTableIfExists("fields");
};
