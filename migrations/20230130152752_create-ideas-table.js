
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = (knex) =>
  knex.schema.createTableIfNotExists("ideas", (ideas) => {
    ideas.increments("ideaId").notNullable();
    ideas.string("createdAt").notNullable();
    ideas.string("office").notNullable();
    ideas.string("title").notNullable();
    ideas.string("keyWords").notNullable();
    ideas.string("challenge").notNullable();
    ideas.string("description").notNullable();
    ideas.string("timeAndPlace").notNullable();
    ideas.string("result").notNullable();
    ideas.string("recommendation").notNullable();
    ideas.integer("ownerId").notNullable()
    ideas.integer("fieldId").notNullable()
    ideas.string("attachFile")
  });

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = (knex) => {
  knex.schema.dropTableIfExists("ideas");
};
