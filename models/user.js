const { Model } = require("objection");
const db = require('../db/db');

class User extends Model {
  static tableName = "users";
  static idColumn = "userId";

  static knex() {
    return db;
  }
}

module.exports = User;
