const { Model } = require('objection')
const db = require('../db/db');

class Person extends Model {
  static tableName = 'people'
  static idColumn = 'personId'

  static knex () {
    return db
  }
}

module.exports = Person
