const { Model } = require('objection')
const db = require('../db/db');

class Field extends Model {
  static tableName = 'fields'
  static idColumn = 'fieldId'

  static knex () {
    return db
  }
}

module.exports = Field
