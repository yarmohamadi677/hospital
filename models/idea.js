const { Model } = require('objection')
const db = require('../db/db');
const Field = require("../models/field");


class Idea extends Model {
  static tableName = 'ideas'
  static idColumn = 'ideaId'
  static relationMappings = {
    field: {
      relation: Model.HasOneRelation,
      modelClass: Field,
      join: {
        from: 'ideas.fieldId',
        to: 'fields.fieldId'
      }
    }
  };

  static knex () {
    return db
  }
}

module.exports = Idea
